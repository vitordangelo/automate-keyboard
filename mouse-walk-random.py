import time
import uinput
import random


events = (
    uinput.REL_X,
    uinput.REL_Y,
    uinput.BTN_LEFT,
    uinput.BTN_RIGHT,
    )

with uinput.Device(events) as device:
    for i in range(20):
        x = random.randint(-500, 500)
        y = random.randint(-500, 500)
        device.emit(uinput.REL_X, x)
        device.emit(uinput.REL_Y, y)
        time.sleep(1)
